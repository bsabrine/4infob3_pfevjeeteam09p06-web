package edu.app.business;
import java.util.List;

import javax.ejb.Remote;

import edu.app.persistance.Experience;

@Remote
public interface ExperienceRemote {
	
	void create(Experience experience);
	Experience findById(int id_experience);
	void update(Experience experience);
	void remove(Experience experience);
	List<Experience> findAll();
	List<Experience> findByposition_exp(String position_exp);

}
