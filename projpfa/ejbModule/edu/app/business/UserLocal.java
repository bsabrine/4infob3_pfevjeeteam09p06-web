package edu.app.business;

import java.util.List;

import javax.ejb.Local;

import edu.app.persistance.AdminRH;
import edu.app.persistance.AdminRecrutement;
import edu.app.persistance.Candidat;
import edu.app.persistance.User;

@Local
public interface UserLocal {
	List<User> findAll();
	void create(User user);
	void update(User user);
	void remove(User user);
	User findById(int id);
	public User GetUserByLoginAndPwd(String login, String pwd);
	List<AdminRH> findAllrh();
	List<AdminRecrutement> findAllrecrutement();
	List<Candidat> findCand();
}
