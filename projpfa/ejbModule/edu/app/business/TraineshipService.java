package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistance.Trainship;

/**
 * Session Bean implementation class ProductService
 */
@Stateless
public class TraineshipService implements TraineshipRemote, TraineshipLocal {

	@PersistenceContext
    private EntityManager em;

	/**
     * Default constructor. 
     */
    public TraineshipService() {
        // TODO Auto-generated constructor stub
    }

	
	public void create( Trainship trainship) {
		em.persist(trainship);
	}

	public Trainship findById(int id_tr) {
		return em.find(Trainship.class, id_tr);
	}

	public void update(Trainship trainship) {
		em.merge(trainship);
		
	}

	
	public void remove(Trainship trainship) {
		Trainship managedTraineship = em.merge(trainship);
		em.remove(managedTraineship);
		
	}

	
	@SuppressWarnings("unchecked")
	public List<Trainship> findAll() {
		
		return em.createQuery("select p from Trainship p order by p.establishment asc").getResultList();
	}

	@Override
	public List<Trainship> findByposition_t(String position_t) {
		List<Trainship> toFind = null;
		Query query = em.createQuery("select p from t-traineship p where UPPER(p.t-traineship) like :param");
		query.setParameter("param", "%"+position_t.toUpperCase()+"%");
		toFind = query.getResultList();
		return toFind;
	}

}