package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistance.RecruitmentCandidate;

/**
 * Session Bean implementation class RecruitmentService
 */
@Stateless
public class RecruitmentService implements RecruitmentServiceRemote, RecruitmentServiceLocal {
	
	@PersistenceContext
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public RecruitmentService() {
        // TODO Auto-generated constructor stub
    }
    
    
    
    public List<RecruitmentCandidate> findAll(){
    	
    	return em.createQuery("select r from RecruitmentCandidate r ").getResultList();
    }
    
    public RecruitmentCandidate findByMail(String mail) {

    	String jpql = "select u from RecruitmentCandidate u where u.mail=:mail";
    	RecruitmentCandidate found = null;
    	Query query = em.createQuery(jpql);
    	query.setParameter("mail", mail);
    	try {
    	found = (RecruitmentCandidate) query.getSingleResult();
    	} catch (Exception ex) {
    	ex.printStackTrace();
    	}
    	return found;
    }

}
