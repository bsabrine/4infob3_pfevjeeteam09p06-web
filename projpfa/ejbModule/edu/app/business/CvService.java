package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistance.Cv;




/**
 * Session Bean implementation class CategoryService
 */
@Stateless
public class CvService implements CvRemote, CvLocal {

	@PersistenceContext
	private EntityManager em;
    /**
     * Default constructor. 
     */

    public CvService() {
    	
    }

	@Override
	public void create(Cv cv) {
		em.persist(cv);
		
	}
	@Override
	public Cv findById(int id_cv) {
		return em.find(Cv.class,id_cv);
	}

	@Override
	public void update(Cv cv) {
		em.merge(cv);
		
	}

	@Override
	public void remove(Cv cv) {
		em.remove(em.merge(cv));
		
	}

	
	@Override
	public List<Cv> findAll() {
		// TODO Auto-generated method stub
		return em.createQuery("select c from Cv c").getResultList();
	}
}
