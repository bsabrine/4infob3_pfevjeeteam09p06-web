package edu.app.business;
import java.util.List;

import javax.ejb.Remote;

import edu.app.persistance.Candidat;

import edu.app.persistance.User;

@Remote
public interface UserRemote {
	List<User> findAll();
	void create(User user);
	void update(User user);
	void remove(User user);
	User findById(int id);
	public User GetUserByLoginAndPwd(String login, String pwd);
	 List<Candidat> findCand();
}
