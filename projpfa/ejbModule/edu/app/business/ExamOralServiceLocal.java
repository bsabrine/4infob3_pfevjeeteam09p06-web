package edu.app.business;

import java.util.List;

import javax.ejb.Local;

import edu.app.persistance.ExamOral;

@Local
public interface ExamOralServiceLocal {
	List<ExamOral> findAll();
	void create(ExamOral exm);
	void update(ExamOral exm);
	void remove(ExamOral exm);
	ExamOral findById(int id);
}
