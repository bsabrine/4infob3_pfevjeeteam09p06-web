package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistance.Formation;

/**
 * Session Bean implementation class ProductService
 */
@Stateless
public class FormationService implements FormationRemote, FormationLocal {

	@PersistenceContext
    private EntityManager em;

	/**
     * Default constructor. 
     */
    public FormationService() {
        // TODO Auto-generated constructor stub
    }

	
	public void create( Formation formation) {
		em.persist(formation);
	}
	
	public Formation findById(int id_forma) {
		return em.find(Formation.class, id_forma);
	}

	public void update(Formation formation) {
		em.merge(formation);
		
	}

	
	public void remove(Formation formation) {
		Formation managedFormation = em.merge(formation);
		em.remove(managedFormation);
		
	}

	
	@SuppressWarnings("unchecked")
	public List<Formation> findAll() {
		
		return em.createQuery("select p from Formation p order by p.type_forma asc").getResultList();
	}

	@Override
	public List<Formation> findBytype_forma(String type_forma) {
		List<Formation> toFind = null;
		Query query = em.createQuery("select p from t-formation p where UPPER(p.t-formation) like :param");
		query.setParameter("param", "%"+type_forma.toUpperCase()+"%");
		toFind = query.getResultList();
		return toFind;
	}



	

}