package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistance.Certificat;

/**
 * Session Bean implementation class ProductService
 */
@Stateless
public class CertificatService implements CertificatRemote, CertificatLocal {

	@PersistenceContext
    private EntityManager em;

	/**
     * Default constructor. 
     */
    public CertificatService() {
 
    }

	
	public void create(Certificat cerificat) {
		em.persist( cerificat);
	}

	public Certificat findById(int id_cert) {
		return em.find(Certificat.class, id_cert);
	}

	public void update(Certificat  cerificat) {
		em.merge(cerificat);
		
	}

	
	public void remove(Certificat cerificat) {
		Certificat managedCertificat = em.merge(cerificat);
		em.remove(managedCertificat);
		
	}

	
	public List<Certificat> findAll() {
		
		return em.createQuery("select p from Certificat p ").getResultList();
	}

	@Override
	public List<Certificat> findBytype_cert(String type_cert) {
		List<Certificat> toFind = null;
		Query query = em.createQuery("select p from t-cerificat p where UPPER(p.t-cerificat) like :param");
		query.setParameter("param", "%"+type_cert .toUpperCase()+"%");
		toFind = query.getResultList();
		return toFind;
	}
}