package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistance.Diplome;

/**
 * Session Bean implementation class ProductService
 */
@Stateless
public class DiplomeService implements DiplomeRemote, DiplomeLocal {

	@PersistenceContext
    private EntityManager em;

	/**
     * Default constructor. 
     */
    public DiplomeService() {
        // TODO Auto-generated constructor stub
    }

	
	public void create( Diplome diplome) {
		em.persist( diplome);
	}

	public Diplome findById(int id_dip) {
		return em.find(Diplome.class, id_dip);
	}

	public void update(Diplome  diplome) {
		em.merge(diplome);
		
	}

	
	public void remove(Diplome diplome) {
		Diplome managedDiplome = em.merge( diplome);
		em.remove(managedDiplome);
		
	}

	
	public List<Diplome> findAll() {
		
		return em.createQuery("select p from Diplome p ").getResultList();
	}

	@Override
	public List<Diplome> findBytype_dip(String type_dip) {
		List<Diplome> toFind = null;
		Query query = em.createQuery("select p from t-diplome p where UPPER(p.t-diplome) like :param");
		query.setParameter("param", "%"+type_dip .toUpperCase()+"%");
		toFind = query.getResultList();
		return toFind;
	}

}