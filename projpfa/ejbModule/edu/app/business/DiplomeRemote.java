package edu.app.business;
import java.util.List;

import javax.ejb.Remote;

import edu.app.persistance.Diplome;

@Remote
public interface DiplomeRemote {
	
	void create(Diplome diplome);
	Diplome findById(int id_dip);
	void update(Diplome diplome);
	void remove(Diplome diplome);
	List<Diplome> findAll();
	List<Diplome> findBytype_dip(String type_dip);

}
