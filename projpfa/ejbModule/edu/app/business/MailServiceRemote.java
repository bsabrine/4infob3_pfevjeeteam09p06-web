package edu.app.business;

import javax.ejb.Remote;

@Remote
public interface MailServiceRemote {
	void sendMail(String to, String subject, String messageBoby);

}
