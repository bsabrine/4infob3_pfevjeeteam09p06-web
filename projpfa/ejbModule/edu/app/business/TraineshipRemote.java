package edu.app.business;
import java.util.List;

import javax.ejb.Remote;

import edu.app.persistance.Trainship;

@Remote
public interface TraineshipRemote {
	
	void create(Trainship trainship);
	Trainship findById(int id_tr);
	void update(Trainship trainship);
	void remove(Trainship trainship);
	List<Trainship> findAll();
	List<Trainship> findByposition_t(String position_t);

}
