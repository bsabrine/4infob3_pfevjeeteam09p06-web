package edu.app.business;

import javax.ejb.Local;

@Local
public interface MailServiceLocal {
	void sendMail(String to, String subject, String messageBoby);

}
