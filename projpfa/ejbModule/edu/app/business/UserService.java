package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistance.AdminRH;
import edu.app.persistance.AdminRecrutement;
import edu.app.persistance.Candidat;

import edu.app.persistance.User;

/**
 * Session Bean implementation class ProductService
 */
@Stateless
public class UserService implements UserRemote, UserLocal {

	@PersistenceContext
    private EntityManager em;

	/**
     * Default constructor. 
     */
    public UserService() {
        // TODO Auto-generated constructor stub
    }

	
	public void create(User user) {
		em.persist( user);
	}
	public void update(User user) {
		em.merge(user);
		
	}

	
	public void remove(User user) {
		User managedUser = em.merge(user);
		em.remove(managedUser);
		
	}
	public User findById(int id) {
		return em.find(User.class, id);
	}
	@SuppressWarnings("unchecked")
	public List<User> findAll() {
		
		return em.createQuery("select p from User p ").getResultList();
	}


@Override
public User GetUserByLoginAndPwd(String login, String pwd) {
Query query= em.createQuery("select u from User u where u.login=:login and u.password=:pwd");
query.setParameter("login", login).setParameter("pwd", pwd);
User u;
try{
	u =(User) query.getSingleResult();   
}catch(Exception e){
	return null;
}
	return u;
}


@SuppressWarnings("unchecked")
@Override
public List<AdminRH> findAllrh() {
	return em.createQuery("select rh from AdminRH rh ").getResultList();
}

@SuppressWarnings("unchecked")
@Override
public List<AdminRecrutement> findAllrecrutement() {
	return em.createQuery("select r from AdminRecrutement r ").getResultList();

}


@SuppressWarnings("unchecked")
public List<Candidat> findCand() {
	

	return em.createQuery("select p from Candidate p where p.state='accepted for interview 1'").getResultList();
}
	
}