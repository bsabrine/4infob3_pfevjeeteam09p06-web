package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.app.persistance.Experience;

/**
 * Session Bean implementation class ProductService
 */
@Stateless
public class ExperienceService implements ExperienceRemote, ExperienceLocal {

	@PersistenceContext
    private EntityManager em;

	/**
     * Default constructor. 
     */
    public ExperienceService() {
        // TODO Auto-generated constructor stub
    }

	
	public void create( Experience experience) {
		em.persist(experience);
	}

	public Experience findById(int id_experience) {
		return em.find(Experience.class, id_experience);
	}

	public void update(Experience experience) {
		em.merge(experience);
		
	}

	
	public void remove(Experience experience) {
		Experience managedExperience = em.merge(experience);
		em.remove(managedExperience);
		
	}

	
	public List<Experience> findAll() {
		
		return em.createQuery("select p from Experience p ").getResultList();
	}

	@Override
	public List<Experience> findByposition_exp(String position_exp) {
		List<Experience> toFind = null;
		Query query = em.createQuery("select p from t-experience p where UPPER(p.t-experience) like :param");
		query.setParameter("param", "%"+position_exp .toUpperCase()+"%");
		toFind = query.getResultList();
		return toFind;
	}

}