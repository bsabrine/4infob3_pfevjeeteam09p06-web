package edu.app.business;
import java.util.List;

import javax.ejb.Remote;

import edu.app.persistance.Certificat;

@Remote
public interface CertificatRemote {
	
	void create(Certificat certificat);
	Certificat findById(int id_cert);
	void update(Certificat certificat);
	void remove(Certificat certificat);
	List<Certificat> findAll();
	List<Certificat> findBytype_cert(String type_cert);

}
