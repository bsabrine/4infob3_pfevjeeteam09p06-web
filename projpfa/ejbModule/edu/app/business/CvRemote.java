package edu.app.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistance.Cv;

@Remote
public interface CvRemote {
	
	void create(Cv cv);
	Cv findById(int id_cv);
	void update(Cv cv);
	void remove(Cv  cv);
	List<Cv > findAll();


}
