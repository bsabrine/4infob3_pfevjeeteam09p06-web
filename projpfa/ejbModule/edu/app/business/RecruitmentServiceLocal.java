package edu.app.business;

import java.util.List;

import javax.ejb.Local;

import edu.app.persistance.RecruitmentCandidate;

@Local
public interface RecruitmentServiceLocal {
	 public List<RecruitmentCandidate> findAll();
	 public RecruitmentCandidate findByMail(String mail);

}
