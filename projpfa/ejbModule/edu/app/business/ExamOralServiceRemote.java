package edu.app.business;

import java.util.List;

import javax.ejb.Remote;

import edu.app.persistance.ExamOral;



@Remote
public interface ExamOralServiceRemote {
	List<ExamOral> findAll();
	void create(ExamOral exm);
	void update(ExamOral exm);
	void remove(ExamOral exm);
	ExamOral findById(int id);
}
