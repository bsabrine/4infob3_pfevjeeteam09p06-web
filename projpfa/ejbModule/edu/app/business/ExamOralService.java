package edu.app.business;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import edu.app.persistance.ExamOral;


/**
 * Session Bean implementation class ExamOralService
 */
@Stateless
public class ExamOralService implements ExamOralServiceRemote, ExamOralServiceLocal {
	@PersistenceContext
    private EntityManager em;
    /**
     * Default constructor. 
     */
    public ExamOralService() {
        // TODO Auto-generated constructor stub
    }

	public List<ExamOral> findAll() {
		return em.createQuery("select e from ExamOral e ").getResultList();
	}

	public void create(ExamOral exm) {
		em.persist( exm);
	}

	public void update(ExamOral exm) {
		em.merge(exm);
	}

	public void remove(ExamOral exm) {
		ExamOral managedExamOral = em.merge(exm);
		em.remove(managedExamOral);
	}

	public ExamOral findById(int id) {
		return em.find(ExamOral.class, id);
	}

}
