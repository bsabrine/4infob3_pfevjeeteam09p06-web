package edu.app.business;
import java.util.List;

import javax.ejb.Remote;

import edu.app.persistance.Formation;


@Remote
public interface FormationRemote {
	
	void create(Formation formation);
	Formation findById(int id_forma);
	void update(Formation formation);
	void remove(Formation formation);
	List<Formation> findAll();
	List<Formation> findBytype_forma(String type_forma);

}
