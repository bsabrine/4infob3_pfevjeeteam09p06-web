package edu.app.persistance;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="t_adminRh")
public class AdminRH extends User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6762162980207739005L;
	
	private int adminLevel;
	
	public AdminRH() {
	}

	public AdminRH(String login, String password,int priv, int adminLevel) {
		super(login, password,priv);
		this.setAdminLevel(adminLevel);
		// TODO Auto-generated constructor stub
	}

	public int getAdminLevel() {
		return adminLevel;
	}

	public void setAdminLevel(int adminLevel) {
		this.adminLevel = adminLevel;
	}

	@Override
	public String toString() {
		return "AdminRecruitment [adminLevel=" + adminLevel + "]";
	}
	
	

}
