package edu.app.persistance;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Candidat
 *
 */
@Entity
@Table(name="t_candidat")

public class Candidat implements Serializable {

	
	private int id;
	private String firstname;

	private String lastName;
	private int cin;
	private String statusCivile;
	private int datebirth;
	private String placeBirth;
	private String nationality;
	private String address;
	private int city;
	private String postalCode;
	private String country;
	private int phone;
	private String mail;
	private String state;
	

	private List<RecruitmentCandidate> recs;
	private Cv cv;
	private static final long serialVersionUID = 1L;

	public Candidat() {
		super();
	}   
	@Id 
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getFirstname() {
		return this.firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	@OneToOne(mappedBy="candidat")
	public Cv getCv() {
		return cv;
	}
	public void setCv(Cv cv) {
		this.cv = cv;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getCin() {
		return cin;
	}
	public void setCin(int cin) {
		this.cin = cin;
	}
	public String getStatusCivile() {
		return statusCivile;
	}
	public void setStatusCivile(String statusCivile) {
		this.statusCivile = statusCivile;
	}
	public int getDatebirth() {
		return datebirth;
	}
	public void setDatebirth(int datebirth) {
		this.datebirth = datebirth;
	}
	public String getPlaceBirth() {
		return placeBirth;
	}
	public void setPlaceBirth(String placeBirth) {
		this.placeBirth = placeBirth;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getCity() {
		return city;
	}
	public void setCity(int city) {
		this.city = city;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public Candidat(String firstname, String lastName, int cin,
			String statusCivile, int datebirth, String placeBirth,
			String nationality, String address, int city, String postalCode,
			String country, int phone, String mail) {
		super();
		this.firstname = firstname;
		this.lastName = lastName;
		this.cin = cin;
		this.statusCivile = statusCivile;
		this.datebirth = datebirth;
		this.placeBirth = placeBirth;
		this.nationality = nationality;
		this.address = address;
		this.city = city;
		this.postalCode = postalCode;
		this.country = country;
		this.phone = phone;
		this.mail = mail;
	
	}
	@Override
	public String toString() {
		return "Candidat [id=" + id + ", firstname=" + firstname
				+ ", lastName=" + lastName + ", cin=" + cin + ", statusCivile="
				+ statusCivile + ", datebirth=" + datebirth + ", placeBirth="
				+ placeBirth + ", nationality=" + nationality + ", address="
				+ address + ", city=" + city + ", postalCode=" + postalCode
				+ ", country=" + country + ", phone=" + phone + ", mail="
				+ mail + ", cv=" + cv + "]";
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	
	
	@OneToMany(mappedBy="candidat")
	public List<RecruitmentCandidate> getRecs() {
		return recs;
	}
	public void setRecs(List<RecruitmentCandidate> recs) {
		this.recs = recs;
	}
   
}
