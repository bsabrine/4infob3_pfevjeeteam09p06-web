package edu.app.persistance;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;


import javax.persistence.*;

/**
 * Entity implementation class for Entity: RecruitmentCandidate
 *
 */
@Entity
@Table(name="t_recruitmentcandidate")

public class RecruitmentCandidate implements Serializable {

	
	private int id;
	private String login;
	private String password;
	private Date date;
	private String mail;
	private Candidat candidat;
	private ExamOral examOral;
	private static final long serialVersionUID = 1L;

	public RecruitmentCandidate() {
		
	}   
	@Id   
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}   
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}   
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	public String toString() {
		return "RecruitmentCandidate [id=" + id + ", login=" + login
				+ ", password=" + password + ", date=" + date + "]";
	}
	
	
	@ManyToOne
	@JoinColumn(name="candidat_fk")
	public Candidat getCandidat() {
		return candidat;
	}
	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	@ManyToOne
	@JoinColumn(name="examoral_fk")
	public ExamOral getExamOral() {
		return examOral;
	}
	public void setExamOral(ExamOral examOral) {
		this.examOral = examOral;
	}
	
	
   
}
