package edu.app.persistance;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: Candidate
 *
 */
@Entity
@Table(name="t_certificat")

public class Certificat implements Serializable {

	
	private int id_cert;
	private String type_cert;
	private String year_dip;
	private Cv cv;
	private static final long serialVersionUID = 1L;

	
	
	
	
	public Certificat() {
		super();
	}







	public Certificat(String type_cert, String year_dip) {
		super();
		this.type_cert = type_cert;
		this.year_dip = year_dip;
	}




	@Id
	public int getId_cert() {
		return id_cert;
	}





	public void setId_cert(int id_cert) {
		int oldId_cert = this.id_cert;
		this.id_cert = id_cert;
		changeSupport.firePropertyChange("id_cert", oldId_cert, id_cert);
	}





	public String getType_cert() {
		return type_cert;
	}

	public void setType_cert(String type_cert) {
		String oldType_cert = this.type_cert;
		this.type_cert = type_cert;
		changeSupport.firePropertyChange("type_cert", oldType_cert, type_cert);
	}





	public String getYear_dip() {
		return year_dip;
	}





	public void setYear_dip(String year_dip) {
		String oldYear_dip = this.year_dip;
		this.year_dip = year_dip;
		changeSupport.firePropertyChange("year_dip", oldYear_dip, year_dip);
	}

	@ManyToOne
	@JoinColumn(name = "cv_fk")
	public Cv getCv() {
		return cv;
	}

	public void setCv(Cv cv) {
		Cv oldCv = this.cv;
		this.cv = cv;
		changeSupport.firePropertyChange("cv", oldCv, cv);
	}
	
	@Transient
	public PropertyChangeSupport getChangeSupport() {
		return changeSupport;
	}




	public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}

	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }




	
	
}
