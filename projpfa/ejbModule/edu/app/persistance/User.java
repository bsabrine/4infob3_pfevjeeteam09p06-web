package edu.app.persistance;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "t_user")
public class User implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1014505330615266768L;
	
	private int id;
	private String login;
	private String password;
	private static int priv;
	
	public User() {
	}

	public User(String login, String password,int priv) {
		super();
		this.login = login;
		this.password = password;
		User.setPriv(priv);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(unique = true)
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Transient
	public static int getPriv() {
		return priv;
	}

	public static void setPriv(int priv) {
		User.priv = priv;
	}
	
    

}
