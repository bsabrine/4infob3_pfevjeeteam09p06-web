package edu.app.persistance;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ExamOral
 *
 */
@Entity
@Table(name="t_examoral")

public class ExamOral implements Serializable {

	
	private int id;
	private Date date;
	private int notecandidate;
	private List<RecruitmentCandidate> recruitmentCandidates;
	private static final long serialVersionUID = 1L;

	public ExamOral() {
		super();
	} 
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Id    
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}   
	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}   
	public int getNotecandidate() {
		return this.notecandidate;
	}

	public void setNotecandidate(int notecandidate) {
		this.notecandidate = notecandidate;
	}
	@OneToMany(mappedBy="examOral")
	public List<RecruitmentCandidate> getRecruitmentCandidates() {
		return recruitmentCandidates;
	}
	public void setRecruitmentCandidates(List<RecruitmentCandidate> recruitmentCandidates) {
		this.recruitmentCandidates = recruitmentCandidates;
	}
	
}
	
   

