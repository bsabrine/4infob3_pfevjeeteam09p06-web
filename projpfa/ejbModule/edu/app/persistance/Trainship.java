package edu.app.persistance;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


/**
 * Entity implementation class for Entity: Trainship
 *
 */
@Entity
@Table(name="t_traineship")

public class Trainship implements Serializable {

	
	private int id_tr;
	private String establishment;
	private int year_t;
	private String position_t;
	private int duration_t;
	private Cv cv;
	private static final long serialVersionUID = 1L;
	
	public Trainship(int id_tr, String establishment, String password,int year_t,String position_t,int duration_t) {
		super();
		this.id_tr = id_tr;
		this.establishment = establishment;
		this.position_t = position_t;
		this.duration_t = duration_t;
	}
	public Trainship() {
	}
	@Id 
	public int getId_tr() {
		return id_tr;
	}
	public void setId_tr(int id_tr) {
		int oldId_tr = this.id_tr;
		this.id_tr = id_tr;
		changeSupport.firePropertyChange("id_tr", oldId_tr, id_tr);
	}
	public String getEstablishment() {
		return establishment;
	}
	public void setEstablishment(String establishment) {
		String oldEstablishment = this.establishment;
		this.establishment = establishment;
		changeSupport.firePropertyChange("establishment", oldEstablishment, establishment);
		
	}
	public int getYear_t() {
		return year_t;
	}
	public void setYear_t(int year_t) {
		int oldYear_t = this.year_t;
		this.year_t = year_t;
		changeSupport.firePropertyChange("year_t", oldYear_t, year_t);
	}
	public String getPosition_t() {
		return position_t;
	}
	public void setPosition_t(String position_t) {
		String oldPosition_t = this.position_t;
		this.position_t = position_t;
		changeSupport.firePropertyChange("position_t", oldPosition_t, position_t);
		
	}
	public int getDuration_t() {
		return duration_t;
	}
	public void setDuration_t(int duration_t) {
		int oldDuration_t = this.duration_t;
		this.duration_t = duration_t;
		changeSupport.firePropertyChange("duration_t", oldDuration_t, duration_t);
		
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + duration_t;
		result = prime * result
				+ ((establishment == null) ? 0 : establishment.hashCode());
		result = prime * result + id_tr;
		result = prime * result
				+ ((position_t == null) ? 0 : position_t.hashCode());
		result = prime * result + year_t;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trainship other = (Trainship) obj;
		if (duration_t != other.duration_t)
			return false;
		if (establishment == null) {
			if (other.establishment != null)
				return false;
		} else if (!establishment.equals(other.establishment))
			return false;
		if (id_tr != other.id_tr)
			return false;
		if (position_t == null) {
			if (other.position_t != null)
				return false;
		} else if (!position_t.equals(other.position_t))
			return false;
		if (year_t != other.year_t)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Trainship [id_tr=" + id_tr + ", establishment="
				+ establishment + ", year_t=" + year_t + ", position_t="
				+ position_t + ", duration_t=" + duration_t + "]";
	} 
	@ManyToOne
	@JoinColumn(name = "cv_fk")
	public Cv getCv() {
		return cv;
	}

	public void setCv(Cv cv) {
		Cv oldCv = this.cv;
		this.cv = cv;
		changeSupport.firePropertyChange("cv", oldCv, cv);
	}
	
	@Transient
	public PropertyChangeSupport getChangeSupport() {
		return changeSupport;
	}




	public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}

	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
}
