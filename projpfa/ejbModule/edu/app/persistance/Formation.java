package edu.app.persistance;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: Candidate
 *
 */
@Entity
@Table(name="t_formation")

public class Formation implements Serializable {

	
	private int id_forma;
	private String type_forma;
	private String year_forma;
	private int duration_forma;
	private Cv cv;
	private static final long serialVersionUID = 1L;

	
	
	
	
	public Formation() {
		super();
	}
	
	public Formation(String type_forma, String year_forma,
			int duration_forma) {
		super();
	
		this.type_forma = type_forma;
		this.year_forma = year_forma;
		this.duration_forma = duration_forma;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId_forma() {
		return id_forma;
	}

	public void setId_forma(int id_forma) {
		int oldId_forma = this.id_forma;
		this.id_forma = id_forma;
		changeSupport.firePropertyChange("id_forma ", oldId_forma, id_forma );
	}

	public String getType_forma() {
		return type_forma;
	}

	public void setType_forma(String type_forma) {
		this.type_forma = type_forma;
	}

	public String getYear_forma() {
		return year_forma;
	}
	public void setYear_forma(String year_forma) {
		String oldYear_forma = this.year_forma;
		this.year_forma = year_forma;
		changeSupport.firePropertyChange("year_forma", oldYear_forma, year_forma);
	}

	public int getDuration_forma() {
		return duration_forma;
	}
	public void setDuration_forma(int duration_forma) {
		int oldDuration_forma = this.duration_forma;
		this.duration_forma = duration_forma;
		changeSupport.firePropertyChange("duration_forma", oldDuration_forma, duration_forma);
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + duration_forma;
		result = prime * result + id_forma;
		result = prime * result
				+ ((type_forma == null) ? 0 : type_forma.hashCode());
		result = prime * result
				+ ((year_forma == null) ? 0 : year_forma.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Formation other = (Formation) obj;
		if (duration_forma != other.duration_forma)
			return false;
		if (id_forma != other.id_forma)
			return false;
		if (type_forma == null) {
			if (other.type_forma != null)
				return false;
		} else if (!type_forma.equals(other.type_forma))
			return false;
		if (year_forma == null) {
			if (other.year_forma != null)
				return false;
		} else if (!year_forma.equals(other.year_forma))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Formation [id_forma=" + id_forma + ", type_forma=" + type_forma
				+ ", year_forma=" + year_forma + ", duration_forma="
				+ duration_forma + "]";
	}  

	@ManyToOne
	@JoinColumn(name = "cv_fk")
	public Cv getCv() {
		return cv;
	}

	public void setCv(Cv cv) {
		Cv oldCv = this.cv;
		this.cv = cv;
		changeSupport.firePropertyChange("cv", oldCv, cv);
	}
	
	@Transient
	public PropertyChangeSupport getChangeSupport() {
		return changeSupport;
	}




	public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}

	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
	
	
}
