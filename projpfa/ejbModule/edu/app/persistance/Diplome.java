package edu.app.persistance;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: Candidate
 *
 */
@Entity
@Table(name="t_diplome")

public class Diplome implements Serializable {

	
	private int id_dip;
	private String type_dip;
	private String year_dip;
	private Cv cv;
	private static final long serialVersionUID = 1L;

	
	
	
	
	public Diplome() {
		super();
	}




	public Diplome(String type_dip, String year_dip) {
		super();
		this.type_dip = type_dip;
		this.year_dip = year_dip;
	}




	@Id
	public int getId_dip() {
		return id_dip;
	}





	public void setId_dip(int id_dip) {
		int oldId_dip = this.id_dip;
		this.id_dip = id_dip;
		changeSupport.firePropertyChange("id_dip", oldId_dip, id_dip);
	}





	public String getType_dip() {
		return type_dip;
	}





	public void setType_dip(String type_dip) {
		String oldType_dip = this.type_dip;
		this.type_dip = type_dip;
		changeSupport.firePropertyChange("type_dip", oldType_dip, type_dip);
	}





	public String getYear_dip() {
		return year_dip;
	}





	public void setYear_dip(String year_dip) {
		String oldYear_dip = this.year_dip;
		this.year_dip = year_dip;
		changeSupport.firePropertyChange("year_dip", oldYear_dip, year_dip);
	}




	@ManyToOne
	@JoinColumn(name = "cv_fk")
	public Cv getCv() {
		return cv;
	}




	public void setCv(Cv cv) {
		Cv oldCv = this.cv;
		this.cv = cv;
		changeSupport.firePropertyChange("cv", oldCv, cv);
	}




	public static long getSerialversionuid() {
		return serialVersionUID;
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id_dip;
		result = prime * result
				+ ((type_dip == null) ? 0 : type_dip.hashCode());
		result = prime * result
				+ ((year_dip == null) ? 0 : year_dip.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Diplome other = (Diplome) obj;
		if (id_dip != other.id_dip)
			return false;
		if (type_dip == null) {
			if (other.type_dip != null)
				return false;
		} else if (!type_dip.equals(other.type_dip))
			return false;
		if (year_dip == null) {
			if (other.year_dip != null)
				return false;
		} else if (!year_dip.equals(other.year_dip))
			return false;
		return true;
	}




	@Override
	public String toString() {
		return "Diplome [id_dip=" + id_dip + ", type_dip=" + type_dip
				+ ", year_dip=" + year_dip + "]";
	}
	
	@Transient
	public PropertyChangeSupport getChangeSupport() {
		return changeSupport;
	}




	public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}

	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
}
