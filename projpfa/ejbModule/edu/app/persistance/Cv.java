package edu.app.persistance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity implementation class for Entity: Candidate
 * 
 */
@Entity
@Table(name = "t_cv")
public class Cv implements Serializable {

	private int idCv;
	
	private Candidat candidat;
	private List<Trainship> traineships;
	private List<Experience> experiences;
	private List<Formation> formations;
	private List<Diplome> diplomes;
	private static final long serialVersionUID = 1L;

	public Cv() {
		super();
	}
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
public int getIdCv() {
	return idCv;
}
public void setIdCv(int idCv) {
	this.idCv = idCv;
}

	
	
	@OneToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE})
	@JoinColumn( name = "candidat_FK",unique = true )
	public Candidat  getCandidat() {
		return candidat;
	}
	public void setCandidat(Candidat candidat) {
		this.candidat = candidat;
	}
	
	@OneToMany(mappedBy = "cv", cascade = { CascadeType.PERSIST,
			CascadeType.MERGE })
	public List<Diplome> getDiplomes() {
		if (diplomes == null)
			diplomes = new ArrayList<Diplome>();
		return diplomes;
	}
	
	public void setDiplomes(List<Diplome> diplomes) {
		this.diplomes = diplomes;
	}
	public void addDiplome(Diplome diplome) {
		diplome.setCv(this);
		this.getDiplomes().add(diplome);
	}
	
	@OneToMany(mappedBy = "cv", cascade = { CascadeType.PERSIST,
			CascadeType.MERGE })
	public List<Trainship> getTraineships() {
		return traineships;
	}

	public void setTraineships(List<Trainship> traineships) {
		this.traineships = traineships;
	}
	public void addTrainship(Trainship trainship){
		trainship.setCv(this );
		   this.getTraineships().add(trainship);
}
	
	
	@OneToMany(mappedBy = "cv", cascade = { CascadeType.PERSIST,
			CascadeType.MERGE })
	public List<Experience> getExperiences() {
		return experiences;
	}

	public void setExperiences(List<Experience> experiences) {
		this.experiences = experiences;
	}
	public void addExperience(Experience experience){
		experience.setCv(this );
		   this.getExperiences().add(experience);
}
	@OneToMany(mappedBy = "cv", cascade = { CascadeType.PERSIST,
			CascadeType.MERGE })
	public List<Formation> getFormations() {
		return formations;
	}

	public void setFormations(List<Formation> formations) {
		this.formations = formations;
	}
	public void addFormation(Formation formation){
		formation.setCv(this );
		   this.getFormations().add(formation);
		   
}
	public void addFormationsToCv(List<Formation> formations){
		this.formations=formations;
		for(Formation f:formations)
			f.setCv(this);
	}
	public void addExperiencesToCv(List<Experience> experiences){
		this.experiences=experiences;
		for(Experience e:experiences){
			e.setCv(this);
		}
		
	}
}
