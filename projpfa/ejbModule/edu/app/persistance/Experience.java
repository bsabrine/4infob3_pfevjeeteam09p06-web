package edu.app.persistance;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Entity implementation class for Entity: Candidate
 *
 */
@Entity
@Table(name="t_experience")

public class Experience implements Serializable {

	
	private int id_experience;
	private String establishment_exp;
	private String year_exp;
	private int duration_exp;
	private String position_exp;
	private Cv cv;
	private static final long serialVersionUID = 1L;

	
	
	
	
	public Experience() {
		super();
	}


	public Experience( String establishment_exp,
			String year_exp, int duration_exp, String position_exp) {
		super();

		this.establishment_exp = establishment_exp;
		this.year_exp = year_exp;
		this.duration_exp = duration_exp;
		this.position_exp = position_exp;
	}

















	@Id  @GeneratedValue(strategy=GenerationType.IDENTITY)
	public int getId_experience() {
		return id_experience;
	}





	public void setId_experience(int id_experience) {
		int oldId_experience = this.id_experience;
		this.id_experience = id_experience;
		changeSupport.firePropertyChange("id_experience", oldId_experience, id_experience);
	}





	public String getEstablishment_exp() {
		return establishment_exp;
	}





	public void setEstablishment_exp(String establishment_exp) {
		String oldEstablishment_exp = this.establishment_exp;
		this.establishment_exp = establishment_exp;
		changeSupport.firePropertyChange("establishment_exp", oldEstablishment_exp, establishment_exp);
	}





	public String getYear_exp() {
		return year_exp;
	}





	public void setYear_exp(String year_exp) {
		String oldYear_exp = this.year_exp;
		this.year_exp = year_exp;
		changeSupport.firePropertyChange("year_exp", oldYear_exp, year_exp);
	}





	public int getDuration_exp() {
		return duration_exp;
	}





	public void setDuration_exp(int duration_exp) {
		int oldDuration_exp = this.duration_exp;
		this.duration_exp = duration_exp;
		changeSupport.firePropertyChange("duration_exp", oldDuration_exp, duration_exp);
	}





	public String getPosition_exp() {
		return position_exp;
	}





	public void setPosition_exp(String position_exp) {
		String oldPosition_exp = this.position_exp;
		this.position_exp = position_exp;
		changeSupport.firePropertyChange("position_exp", oldPosition_exp, position_exp);
	}





	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + duration_exp;
		result = prime
				* result
				+ ((establishment_exp == null) ? 0 : establishment_exp
						.hashCode());
		result = prime * result + id_experience;
		result = prime * result
				+ ((position_exp == null) ? 0 : position_exp.hashCode());
		result = prime * result
				+ ((year_exp == null) ? 0 : year_exp.hashCode());
		return result;
	}



	@ManyToOne
	@JoinColumn(name = "cv_fk")
	public Cv getCv() {
		return cv;
	}




	public void setCv(Cv cv) {
		Cv oldCv = this.cv;
		this.cv = cv;
		changeSupport.firePropertyChange("cv", oldCv, cv);
	}
	

	@Transient
	public PropertyChangeSupport getChangeSupport() {
		return changeSupport;
	}




	public void setChangeSupport(PropertyChangeSupport changeSupport) {
		this.changeSupport = changeSupport;
	}

	private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
	
	
}
